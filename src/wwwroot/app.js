var fohmDevTest = angular.module('fohmDevTest', []);

fohmDevTest.controller('FohmDevTestController', function FohmDevTestController($scope, $http) {
    let incUrl = 'api/inc';
    let decUrl = 'api/dec';
    let addPersonUrl = 'api/addPerson';

    $scope.number = 1;
    $scope.title = 'Fohm Dev Test';
    $scope.name = '';
    $scope.increment = function() {
        $http.post(incUrl, $scope.number).then(function(response) {
            $scope.number = response.data;
        })
    };
    $scope.decrement = function() {
        $http.post(decUrl, $scope.number).then(function(response) {
            $scope.number = response.data;
        })
    };
    $scope.submitName = function() {
        $http.post(addPersonUrl, {
            number: $scope.number,
            name: $scope.name
        }).then(function(response) {})
    };
});

fohmDevTest.controller('GetPeopleController', function GetPeopleController($scope, $http) {
    let getPeopleUrl = 'api/people';
    $scope.people = [];

    $scope.getPeople = function() {
        $http.get(getPeopleUrl).then(function(response) {
            $scope.people = response.data;
        })
    }
})