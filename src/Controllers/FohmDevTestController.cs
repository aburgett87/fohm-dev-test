﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fohm.DevTest.Api.Data;
using Fohm.DevTest.Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fohm.DevTest.Controllers
{
    [Route("api")]
    public class FohmDevTestController : Controller
    {
        readonly PeopleContext _context;
        public FohmDevTestController(PeopleContext context)
        {
            _context = context;

        }
        // POST api/inc
        [HttpPost("inc")]
        public int Increment([FromBody]int value)
        {
            return value + 1;
        }

        // POST api/dec
        [HttpPost("dec")]
        public int Decrement([FromBody]int value)
        {
            return value - 1;
        }

        [HttpPost("addPerson")]
        public IActionResult AddPerson([FromBody]Person person)
        {
            var newPerson = _context.Add(person);
            _context.SaveChanges();
            return Created($"api/addPerson/{newPerson.Entity.Id}", person);
        }

        [HttpGet("people")]
        public List<Person> GetPeople()
        {
            return _context.People.ToList();
        }
    }
}
