namespace Fohm.DevTest.Api.Models
{
    public class Person
    {
        public int Id {get;set;}
        public string Name {get;set;}
        public int Number {get;set;}
    }
}