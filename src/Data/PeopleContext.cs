using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Fohm.DevTest.Api.Models;

namespace Fohm.DevTest.Api.Data
{
    public class PeopleContext : DbContext
    {
        public DbSet<Person> People {get;set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=people.db");
        }
    }
}