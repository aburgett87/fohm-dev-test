# Fohm Dev Test

## Overview
The Fohm Dev Test Application is an ASP.Net Core 1.1 API with an AngularJS frontend

## Running
Perform the following commnands in a terminal to execute the test

```
cd src
dotnet restore
dotnet run
```

Navigate to http://localhost:5000/ for the main test application. If you wish to
see the names you have entered go to http://localhost:5000/list.html